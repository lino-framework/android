package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.network.UrlSource
import com.example.myapplication.network.WebAppInterface

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.domStorageEnabled = true
//        binding.webView.addJavascriptInterface(WebAppInterface(this), "Android")
        binding.webView.webViewClient = WebViewClient()
        binding.jane.setOnClickListener {
            binding.sites.visibility = View.INVISIBLE
            binding.webView.visibility = View.VISIBLE
            supportActionBar?.hide()
            binding.webView.loadUrl(UrlSource.jane)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && binding.sites.visibility != View.VISIBLE) {
            if (binding.webView.canGoBack()) {
                binding.webView.goBack()
            } else {
                binding.sites.visibility = View.VISIBLE
                binding.webView.visibility = View.INVISIBLE
                supportActionBar?.show()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}