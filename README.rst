
=====================
Lino android packages
=====================

We currently have a simple android package which consist of mainly a webview.
You can download the android package (.apk) from
`<https://gitlab.com/lino-framework/android/-/blob/master/lino_android/lino_webview/app/release/app-release.apk>`__,
and install it on your phone.
